<?php
/**
 * Sets up initial data when a plugin is activated.
 *
 * @return void
 */
function ccr_activate() {

	if ( ! get_option( 'ccr_settings' ) ) {
		$product_categories = woo_product_categories();

		$affiliate_groups = get_active_affiliate_groups();

		$arr = array();

		foreach ( $product_categories as $category ) {
			foreach ( $affiliate_groups as $group ) {
				$arr[ $category->name ][ $group['name'] ] = 0;
			}
		}

		update_option( 'ccr_settings', $arr );

	}
}
