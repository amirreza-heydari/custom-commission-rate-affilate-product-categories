<?php

/**
 * Registers the Settings Page.
 *
 * @return void
 */
function ccr_register_page() {

	add_menu_page(
		__( 'Commission Settings', 'ccr-gpc' ),
		'Commission Settings for Groups and Categories',
		'manage_options',
		'commission-settings-for-affiliates',
		'display_ccr_page',
	);

}
