<?php
/**
 * Displays the Settings Page.
 *
 * @return void
 */
function display_ccr_page() {

	$product_categories = woo_product_categories();

	$affiliate_groups = get_active_affiliate_groups();

	$ccr_settings = get_option( 'ccr_settings' );
	?>
	<div class="custom-commission-settings-div" >
	<div class="jumbotron text-center">
	<h1><?php esc_html_e( 'Commission Settings for Groups and Categories', 'ks-cpm' ); ?></h1>

	</div>
	<div class="container" >
		<form  id="ccr-commission-settings-form" style="min-width:100%;" >
		<div id="ccr-message" class="text-center text-white"></div>

	<input type="hidden" name='action' value='process_ccr_settings_form'>
			<?php wp_nonce_field( 'ccr_nonce_verify' ); ?>
		<div class="row">
		<?php
		foreach ( $product_categories as $category ) {
			?>
<div class="card col-sm-4" style="min-width:48%;margin:1%;justify-content:space-between;">

<div class="card-body" style="margin-top:2%;">
<div id="custom-commission-settings-inner-body" >
<div class="row text-center">
<div  class="col-sm-12 " ><h1><?php echo $category->name; ?></h1> </h1>
</div>

<div  class="col-sm-6 " ><h2><?php esc_html_e( 'Groups', 'ks-cpm' ); ?> </h2>
</div>
<div  class="col-sm-6 "> <h2><?php esc_html_e( 'Percentage', 'ks-cpm' ); ?> </h2> </div>
</div>
			<?php
			// Looping and creating HTML.
			foreach ( $affiliate_groups as $affiliate_group ) {
				$value = isset( $ccr_settings[ $category->name ][ $affiliate_group['name'] ] ) ? $ccr_settings[ $category->name ][ $affiliate_group['name'] ] : 0;

				?>
<div class="row text-center form-group">
<div  class="col-sm-6 " ><label for="id-<?php echo $affiliate_group['name']; ?>"></label> <h2><?php echo esc_html( $affiliate_group['name'] ); ?> </h2>
</div>
<div  class="col-sm-6 "> <input style="max-width:95%;" step="0.01" type="number" class="form-control" id="id-<?php echo esc_html( $affiliate_group['name'] ); ?>" value="<?php echo esc_html( $value ); ?>" name="ccr_settings[<?php echo esc_html( $category->name ); ?>][<?php echo esc_html( $affiliate_group['name'] ); ?>]"> </div>
</div>
				<?php
			}

			?>

		</div>
</div>

</div>	
			<?php
		}
		?>
</div>
<div class="row text-center justify-content-center" style="margin-top:2%">
		<button type="submit" class="btn-lg bg-success text-white" ><?php esc_html_e( 'Submit', 'ks-cpm' ); ?></button>
		</div>
		</form>
</div>
</div>
			<?php
}
