<?php
/**
 * Registers and Enqueues Scripts and Styles
 *
 * @return void
 */
function ccr_enqueue() {
		// Register Styles.
	wp_register_style( 'ccr_bootstrap', plugins_url( 'assets/css/bootstrap.css', CCR_PATH ), array(), time() );
	wp_register_style( 'ccr_custom', plugins_url( 'assets/css/custom.css', CCR_PATH ), array(), time() );

	// Enqueue Styles.
	wp_enqueue_style( 'ccr_bootstrap' );
	wp_enqueue_style( 'ccr_custom' );

	// Register Scripts.

	wp_register_script( 'ccr_main', plugins_url( 'assets/js/main.js', CCR_PATH ), array(), time(), true );

	// Enqueue Scripts.

	wp_enqueue_script( 'ccr_main' );

	wp_localize_script( 'ccr_main', 'ccr_obj', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
