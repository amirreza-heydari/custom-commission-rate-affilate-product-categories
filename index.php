<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/*
Plugin Name: Custom Commission Rate for AF Groups and Product Categories
Plugin URI: https://gitlab.com/amirreza-heydari/custom-commission-rate-affilate-product-categories
Description: The Plugin allows you to award different commission rate based on user category and Affilliate Groups.
Version: 1.0
Author: Rehan Khan
Author URI: https://www.codeable.io/developers/aman-khan/
textdomain:ccr-gpc
*/

define( 'CCR_PATH', __FILE__ );

// Include.
require 'activation.php';
require 'admin/register-ccr-page.php';
require 'admin/display-ccr-page.php';
require 'admin/enqueue.php';
require 'utilities/woo-product-categories.php';
require 'process/process-ccr-settings-form.php';
require 'process/update-referral-amount.php';
require 'process/initial-category-data.php';
require 'utilities/core-plugins-required.php';

if ( in_array( 'affiliate-wp-affiliate-groups/affiliate-wp-affiliate-groups.php', get_option( 'active_plugins' ), true ) &&
	in_array( 'affiliate-wp/affiliate-wp.php', get_option( 'active_plugins' ), true ) ) {

	// Actions.
	register_activation_hook( CCR_PATH, 'ccr_activate' );
	add_action( 'admin_menu', 'ccr_register_page' );
	add_action( 'admin_enqueue_scripts', 'ccr_enqueue', 100 );
	add_action( 'wp_ajax_process_ccr_settings_form', 'process_ccr_settings_form' );
	add_action( 'create_product_cat', 'add_initial_data' );

	// Filter.
	add_filter( 'affwp_calc_referral_amount', 'ccr_calc_referral_amount', 1000, 5 );


	// Shortcodes.


} else {
	add_action( 'admin_notices', 'core_plugins_required' );

}

