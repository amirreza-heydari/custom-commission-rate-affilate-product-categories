<?php
/**
 * Adds initial data when a new category is added.
 *
 * @return void
 */
function add_initial_data( $term_id ) {

	$term_name = get_term( $term_id )->name;

	$ccr_settings = get_option( 'ccr_settings' );

	$product_categories = woo_product_categories();

	$affiliate_groups = get_active_affiliate_groups();

	$arr = $ccr_settings;

	foreach ( $affiliate_groups as $group ) {
		$arr[ $term_name ][ $group['name'] ] = 10;
	};

	update_option( 'ccr_settings', $arr );
}
