<?php

use Wdr\App\Controllers\ManageDiscount;
/**
 * Calculate Referral Amount.
 *
 * @return void
 */
function ccr_calc_referral_amount( $referral_amount, $affiliate_id, $amount, $reference, $product_id ) {

	$ccr_settings = get_option( 'ccr_settings' );

	$affiliate_groups   = get_affiliates_active_groups( $affiliate_id );
	$product_categories = get_the_terms( $product_id, 'product_cat' );

	if ( empty( $affiliate_groups ) || empty( $product_categories ) ) {

		return $referral_amount;
	}

	$arr = array();
	foreach ( $product_categories as $category ) {
		foreach ( $affiliate_groups as $group ) {
			if ( isset( $ccr_settings[ $category->name ][ $group ] ) ) {
				$arr[] = $ccr_settings[ $category->name ][ $group ];
			} else {
				$arr[] = 0;
			}
		}
	}

	$final = max( $arr );

	if ( $final == 0 ) {
		return $referral_amount;
	}

	// $ccr_test = get_option( 'ccr_test', array() );

	if ( in_array( 'woo-discount-rules/woo-discount-rules.php', get_option( 'active_plugins' ), true ) ) {

		$_product = wc_get_product( $product_id );
		$discount = ManageDiscount::calculateProductDiscountPrice(
			$_product->get_price(),
			$product_id
		);

		if ( ! empty( $discount ) ) {
			foreach ( WC()->cart->get_cart() as $cart_item ) {
				if ( in_array( $product_id, array( $cart_item['product_id'], $cart_item['variation_id'] ) ) ) {
					$number_of_products = $cart_item['quantity'];
					break; // stop the loop if product is found
				}
			}

			$orignal_product_commission = $_product->get_price() * $final / 100;

			$discounted_amount = $number_of_products * ( $orignal_product_commission - ( $_product->get_price() - $discount ) );

			// $ccr_test[] = array( $discount, $number_of_products, $orignal_product_commission, $discounted_amount, $final, $amount );

			// update_option( 'ccr_test', $ccr_test );

			if ( $discounted_amount < 0 ) {
				$discounted_amount = 0;
			}

			return $discounted_amount;
		}
	}

	return $amount * $final / 100;
}
