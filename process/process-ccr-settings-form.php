<?php
/**
 * Processes and Saves the data sent by Settings Form.
 *
 * @return void
 */
function process_ccr_settings_form() {

	check_admin_referer( 'ccr_nonce_verify' );
	$response = array( 'status' => 1 );

	if ( ! current_user_can( 'manage_options' ) ) {
		wp_send_json( $response );
	}

	if ( ! isset( $_POST['ccr_settings'] ) ) {
		wp_send_json( $response );
	}

	$ccr_settings = wp_unslash( $_POST['ccr_settings'] );

	update_option( 'ccr_settings', $ccr_settings );

	$response = array( 'status' => 2 );

	wp_send_json( $response );
}
