<?php
/**
 * Displays a message when the core plugins are not available.
 *
 * @return void
 */
function core_plugins_required() {

	?>
	<div class="notice notice-error is-dismissible">
		<p><?php esc_html_e( 'Custom Commission Rate for AF Groups and Product Categories requires Affiliate WP And Groups Addon', 'ccr-gpc' ); ?></p>
	</div>
	<?php
}
